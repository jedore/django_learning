from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views import View

from .forms import LoginForm
from .models import CustomUser


# Create your views here.


class LoginView(View):
    @staticmethod
    def get(request):
        form = LoginForm()
        return render(request, 'login/login.html', locals())

    @staticmethod
    def post(request):
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            # default auth backend
            # user = authenticate(username=username, password=password)

            # custom auth backend
            user = CustomBackend.authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return render(request, 'login/after_login.html', locals())
            else:
                error = 'username or password is wrong.'
                return render(request, 'login/login.html', locals())


class CustomBackend:
    """
    custom authentication backend
    """

    @staticmethod
    def authenticate(username=None, password=None):
        user = CustomUser.objects.get(username=username)
        if user.check_password(password):
            return user
        else:
            return None

    @staticmethod
    def get_user(user_id):
        try:
            return User.objects.get(username=user_id)
        except User.DoesNotExist:
            return None


class LogoutView(View):
    @staticmethod
    def get(request):
        logout(request)
        return redirect('/login')


def next_page(request):
    return render(request, 'login/next.html')


def last_page(request):
    return render(request, 'login/after_login.html')
